function troca(v,i,j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function insercao(v)
	tam = length(v)
	for i in 2 : tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j],v[j-1]) == true
				troca(v,j,j-1)
			else
				break
			end
			j = j - 1
		end
	end
	return v
end


#Parte 1

function compareByValue(x,y)
	a = x[1]
	b = y[1]
	if a >= '2' && a <= '9'
		if b >= '2' && b <= '9'
			if a < b
				return true
			else
				return false
			end
		else
			return true 
		end
	end
	if a == '1'
		if b >= '1' && b <= '9'
			return false
		else
			return true
		end
	end
	if a == 'J'
		if b == 'Q' || b == 'K'|| b == 'A'
			return true
		else
			return false
		end
	end
	if a == 'Q'
		if b == 'K' || b == 'A'
			return true
		else
			return false
		end
	end
	if a == 'K'
		if b == 'A'
			return true
		else
			return false
		end
	end
	if a == 'A'
		return false
	end
end

# Parte 2

function compareByValueAndSuit(x,y)
	c = x[length(x)]
	d = y[length(y)] 
	if c == d 
		return compareByValue(x,y)
	else
		if c == '♢' || c == '♦'
			return true
		elseif c == '♠' 
			if d == '♢' || d == '♦'
				return false
			else
				return true
			end
		elseif c == '♡' || c == '♥'
			if d == '♣'
				return true
			else
				return false
			end
		else 
			return false
		end
	end
end

#Testes

function testValue()
 @test compareByValue("2♡","A♠") == true
 @test compareByValue("K♠","10♠") == false
 @test compareByValue("10♠","10♡") == false
 @test compareByValue("10♡","2♡") == false
end

function testSuit()
 @test compareByValueAndSuit("2♠", "A♠") == true
 @test compareByValueAndSuit("K♥", "10♥") == false
 @test compareByValueAndSuit("10♠", "10♥") == true
 @test compareByValueAndSuit("A♠","2♥") == true
end
